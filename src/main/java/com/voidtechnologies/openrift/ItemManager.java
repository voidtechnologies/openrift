package com.voidtechnologies.openrift;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author dylan
 */
public class ItemManager {

    private HashMap<ItemStack, Double> items = new HashMap<>();
    public static final int COMMON = 20, RARE = 30, EPIC = 35, LEGENDARY = 42;
    public static Double RARE_RATE = 25.0, EPIC_RATE = 12.5, LEGENDARY_RATE = 2.5;
    private List<ItemStack> commonDrops, rareDrops, epicDrops, legendaryDrops;

    public void loadItems(FileConfiguration config) {

        for (String name : config.getConfigurationSection("items").getKeys(false)) {
            Material type;
            Object[] enchants;
            int durability;
            String cName, range;
            Double value;
            Integer amount;

            type = Material.valueOf(config.getString("items." + name + ".material", "STICK"));
            enchants = config.getStringList("items." + name + ".enchants").toArray();
            durability = config.getInt("items." + name + ".durability", -1);
            cName = config.getString("items." + name + ".name", "NONE");
            value = config.getDouble("items." + name + ".value", 1.0);
            range = config.getString("items." + name + ".AMOUNT", "1-1");
            amount = getAmountFromRange(range);

            ItemStack is = new ItemStack(type, amount);
            is.setDurability((short) durability);
            ItemMeta im = is.getItemMeta();
            if (!cName.equalsIgnoreCase("NONE")) {
                im.setDisplayName(cName);
            }

            for (Entry<Enchantment, Integer> enchantment : getEnchants(enchants).entrySet()) {
                im.addEnchant(enchantment.getKey(), enchantment.getValue(), true);
            }

            is.setItemMeta(im);
            items.put(is, value);
        }

        commonDrops = getLootTable(COMMON, 0);
        rareDrops = getLootTable(RARE, COMMON);
        epicDrops = getLootTable(EPIC, RARE);
        legendaryDrops = getLootTable(LEGENDARY, EPIC);
    }

    public static Integer getAmountFromRange(String range) {
        if (range.equalsIgnoreCase("-1")) {
            return 1;
        }
        int high, low;
        Random rand = new Random();
        String[] amts = range.split("-");
        low = Integer.parseInt(amts[0]);
        high = Integer.parseInt(amts[1]);

        return rand.nextInt(1 + high - low) + low;
    }

    public List<ItemStack> getCommonDrops() {
        return commonDrops;
    }

    public List<ItemStack> getRareDrops() {
        return rareDrops;
    }

    public List<ItemStack> getEpicDrops() {
        return epicDrops;
    }

    public List<ItemStack> getLegendaryDrops() {
        return legendaryDrops;
    }

    public static Map<Enchantment, Integer> getEnchants(Object[] enchants) {
        Map<Enchantment, Integer> enchantments = new HashMap<>();
        for (Object objEnchant : enchants) {
            String enchant = objEnchant.toString();
            if (enchant.equalsIgnoreCase("NONE")) {
                continue;
            }
            String[] sep = enchant.split(";");
            Enchantment ench = Enchantment.getByName(sep[0]);
            if (ench == null) {
                System.out.println(sep[0]);
                continue;
            }
            enchantments.put(ench, Integer.valueOf(sep[1]));
        }
        return enchantments;
    }

    public HashMap<ItemStack, Double> getItems() {
        return items;
    }

    public List<ItemStack> getLootTable(int upper, int lower) {
        //<= 2 - Common, <= 3 - Rare, <= 4 = Epic, <= 5
        List<ItemStack> loot = new ArrayList<>();
        for (Entry<ItemStack, Double> entry : items.entrySet()) {
            if (entry.getValue() <= upper && entry.getValue() > lower) {
                loot.add(entry.getKey());
            }
        }
        return loot;
    }
}
