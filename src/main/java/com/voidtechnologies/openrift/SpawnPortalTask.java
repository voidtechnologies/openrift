package com.voidtechnologies.openrift;

import static com.voidtechnologies.openrift.OpenRift.log;
import com.voidtechnologies.openrift.dungeons.RiftRaid;

/**
 *
 * @author dylan
 */
public class SpawnPortalTask implements Runnable {

    private long lastSpawn = 0;
    private OpenRift or;

    public SpawnPortalTask(OpenRift or) {
        this.or = or;
    }

    @Override
    public void run() {
        if (OpenRift.getRift() != null) {
            return;
        }
        log("Rift is null");
        if (System.currentTimeMillis() - lastSpawn >= 900000) {

            if (Math.random() * 100.0 >= 101.0) {
                lastSpawn = System.currentTimeMillis();
                log("Generating rift");
                RiftRaid rift = new RiftRaid("OpenRift_Raid", or);
            } else {
                log("Failed to generate not 95%");
            }
        }
    }

    public void setLastSpawn(long l) {
        lastSpawn = l;
    }
}