package com.voidtechnologies.openrift.listeners;

import com.voidtechnologies.openrift.Attribute;
import com.voidtechnologies.openrift.AttributeType;
import com.voidtechnologies.openrift.OpenRift;
import static com.voidtechnologies.openrift.OpenRift.log;
import com.voidtechnologies.openrift.dungeons.Portal;
import com.voidtechnologies.openrift.dungeons.RiftRaid;
import com.voidtechnologies.openrift.dungeons.SpawnPoint;
import com.voidtechnologies.openrift.mechanics.FireWave;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 *
 * @author dylan
 */
public class ORListener implements Listener {

    private OpenRift or;

    public ORListener(OpenRift or) {
        this.or = or;
        registerEvents();
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void playerDamagedByEntity(EntityDamageEvent event) {
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }
        RiftRaid rift = OpenRift.getRift();
        if (rift == null) {
            return;
        }
        if (!event.getEntity().getLocation().getWorld().equals(Bukkit.getWorld("OpenRift_World"))) {
            return;
        }
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if (!(e.getDamager() instanceof LivingEntity)) {
            return;
        }
        if (e.getDamager() instanceof Player && !(e.getEntity() instanceof Player)) {
            Set<Attribute> attrs = rift.getAttributes().keySet();
            List<Attribute> attributes = Attribute.getAttributes(attrs, AttributeType.ON_HIT);
            for (Attribute attr : attributes) {
                Attribute.applyOnHitAttribute((LivingEntity) e.getDamager(), attr, rift.getAttributes().get(attr), e);
            }
            return;
        }
        if (!(e.getEntity() instanceof Player)) {
            return;
        }

        LivingEntity le = (LivingEntity) e.getDamager();
        if (le instanceof Player) {
            return;
        }
        log("Applying on dmg effects cause damager was not player, and damagee was player");
        Set<Attribute> attrs = rift.getAttributes().keySet();
        List<Attribute> attributes = Attribute.getAttributes(attrs, AttributeType.ON_DAMAGE);
        for (Attribute attr : attributes) {
            Attribute.applyOnDamageAttribute((LivingEntity) e.getEntity(), attr, rift.getAttributes().get(attr), e);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerEnterPortal(PlayerMoveEvent event) {

        Player player = event.getPlayer();
        Location from = event.getFrom();
        Location to = event.getTo();

        if (event.isCancelled()) {
            return;
        }

        if (from.getBlockX() == to.getBlockX()
                && from.getBlockY() == to.getBlockY()
                && from.getBlockZ() == to.getBlockZ()
                && from.getWorld().equals(to.getWorld())) {
            // Player didn't move by at least one block.
            return;
        }
        Portal portal = OpenRift.isInPortal(player);
        if (portal == null) {
            return;
        }

        player.teleport(portal.getExitLocation());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockExplode(BlockExplodeEvent event) {
        if (!event.getBlock().getWorld().equals(Bukkit.getWorld("OpenRift_World"))) {
            return;
        }
        log("Prevented explosion.");
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockDmg(BlockDamageEvent event) {
        if (!event.getBlock().getWorld().equals(Bukkit.getWorld("OpenRift_World"))) {
            return;
        }
        log("Prevented block dmg.");
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        World world = event.getLocation().getWorld();
        if (!world.getName().equalsIgnoreCase("OpenRift_World")) {
            return;
        }
        RiftRaid rift = OpenRift.getRift();
        if (rift == null) {
            return;
        }

        if (!event.getSpawnReason().equals(SpawnReason.CUSTOM)) {
            event.setCancelled(true);
            return;
        }

        List<Attribute> spawnAttrs = Attribute.getAttributes(rift.getAttributes().keySet(), AttributeType.ON_SPAWN);

        for (Attribute attr : spawnAttrs) {
            Attribute.applySpawnAttribute(event.getEntity(), attr, rift.getAttributes().get(attr));
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        if (!event.getBlock().getLocation().getWorld().equals(Bukkit.getWorld("OpenRift_World"))) {
            return;
        }
        event.setCancelled(true);
        log("Prevented block break.");
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityExplode(EntityExplodeEvent event) {
        if (!event.getEntity().getLocation().getWorld().equals(Bukkit.getWorld("OpenRift_World"))) {
            return;
        }
        event.blockList().clear();
        log("Prevented entity explosion breaking blocks.");
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityChangeBlock(EntityChangeBlockEvent event) {
        if (!event.getBlock().getLocation().getWorld().equals(Bukkit.getWorld("OpenRift_World"))) {
            return;
        }
        event.setCancelled(true);
        log("Prevented entity change block.");
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void riftBossDie(final EntityDeathEvent event) {
        LivingEntity boss = event.getEntity();
        if (boss instanceof Player) {
            return;
        }
        if (OpenRift.getRift() == null) {
            return;
        }
        RiftRaid rift = OpenRift.getRift();
        if (!rift.getBosses().contains(boss)) {
            return;
        }
        rift.getBosses().remove(boss);
        if (!rift.getBosses().isEmpty()) {
            return;
        }

        Bukkit.broadcastMessage("The Rift Boss has been killed, the rift is now closing in " + OpenRift.getRift().getCloseTime() + " seconds.");
        or.getSpt().setLastSpawn(System.currentTimeMillis());
        OpenRift.getRift().summonExitPortals(or);
        for (Portal portal : rift.getPortals()) {
            portal.setEnabled(false);
        }

        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(or, new Runnable() {

            @Override
            public void run() {
                OpenRift.getRift().getSchem().eraseSchematic(new Location(event.getEntity().getWorld(), 0, 1, 0));
                OpenRift.getRift().destroy(false);
                OpenRift.setRift(null);
            }
        }, OpenRift.getRift().getCloseTime() * 20L);

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onSpEntityDie(EntityDeathEvent event) {
        if (OpenRift.getRift() == null) {
            return;
        }
        RiftRaid rift = OpenRift.getRift();
        if (event.getEntity() instanceof Player) {
            return;
        }
        LivingEntity le = event.getEntity();
        if (!(event.getEntity().getKiller() instanceof Player)) {
            return;
        }
        UUID uuid = le.getUniqueId();
        if (!le.getWorld().equals(Bukkit.getWorld("OpenRift_World"))) {
            return;
        }
        SpawnPoint sp = SpawnPoint.getSpawnPointByUUID(uuid);
        if (sp == null) {
            return;
        }
        event.getDrops().clear();
        event.setDroppedExp(0);
        sp.getEntities().remove(uuid);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!event.getBlock().getWorld().equals(Bukkit.getWorld("OpenRift_World"))) {
            return;
        }
        event.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void fireBall(PlayerInteractEvent event) {
        
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && !event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            return;
        }
        
        Player player = event.getPlayer();
        
        if (!player.getItemInHand().getType().equals(Material.FLINT_AND_STEEL)) {
            return;
        }
        
        event.setCancelled(true);
        new FireWave((LivingEntity) player).prepareSpell();
    }

    private void registerEvents() {
        Bukkit.getServer().getPluginManager().registerEvents(this, or);
    }
}
