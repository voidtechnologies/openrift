package com.voidtechnologies.openrift.dungeons;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

/**
 *
 * @author dylan
 */
public class SpawnPoint {

    private Location loc;
    private EntityType[] types;
    private int amount, radius, delay, maxAmount, maxHealth, minHealth;
    private Map<UUID, LivingEntity> entities = new HashMap<>();
    private static Map<Location, SpawnPoint> spawns = new HashMap<>();
    private boolean spawning;

    public SpawnPoint(Location loc, EntityType[] types, int amount, int delay, int radius, int maxHealth, int minHealth, int maxAmount) {
        this.loc = loc;
        this.types = types;
        this.amount = amount;
        this.delay = delay;
        this.radius = radius;
        this.maxAmount = maxAmount;
        this.maxHealth = maxHealth;
        this.minHealth = minHealth;
        spawns.put(loc, this);
    }

    public void spawnEntity() {
        if (entities.size() >= amount) {
            return;
        }
        if (maxAmount <= 0) {
            spawns.remove(loc);
            return;
        }
        Random r = new Random();
        Location l = loc.clone();
        l.add(r.nextInt(radius), 0, r.nextInt(radius));
        Location ol = l.clone().add(0, 1, 0);
        while (l.getBlock().getType().isSolid() && ol.getBlock().getType().isSolid()) {
            l = loc.clone();
            l.add(r.nextInt(radius), 0, r.nextInt(radius));
            ol = l.clone().add(0, 1, 0);
        }
        LivingEntity ent = (LivingEntity) loc.getWorld().spawnEntity(l, types[r.nextInt(types.length)]);
        if (minHealth != -1) {
            int health = r.nextInt(maxHealth - minHealth + 1) + minHealth;
            ent.setMaxHealth(health);
            ent.setHealth(health);
        }
        entities.put(ent.getUniqueId(), ent);
        maxAmount--;
        CreatureSpawnEvent e = new CreatureSpawnEvent(ent, SpawnReason.CUSTOM);
        Bukkit.getServer().getPluginManager().callEvent(e);
    }

    public boolean isSpawning() {
        return spawning;
    }

    public void setSpawning(boolean spawning) {
        this.spawning = spawning;
    }

    public Location getLoc() {
        return loc;
    }

    public EntityType[] getTypes() {
        return types;
    }

    public Map<UUID, LivingEntity> getEntities() {
        return entities;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public static SpawnPoint getSpawnPointByUUID(UUID uuid) {
        for (SpawnPoint sp : spawns.values()) {
            if (sp.getEntities().containsKey(uuid)) {
                return sp;
            }
        }
        return null;
    }

    public static Map<Location, SpawnPoint> getSpawns() {
        return spawns;
    }
}
