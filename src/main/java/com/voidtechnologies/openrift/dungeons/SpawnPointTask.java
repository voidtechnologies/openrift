package com.voidtechnologies.openrift.dungeons;

import com.voidtechnologies.openrift.OpenRift;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 *
 * @author dylan
 */
public class SpawnPointTask implements Runnable {

    OpenRift or;

    public SpawnPointTask(OpenRift or) {
        this.or = or;
    }

    @Override
    public void run() {
        Map<Location, SpawnPoint> spawns = SpawnPoint.getSpawns();
        if (OpenRift.getRift() == null) {
            return;
        }
        for (final SpawnPoint sp : spawns.values()) {
            if (sp.isSpawning()) {
                continue;
            }
            if (sp.getEntities().size() < sp.getAmount()) {
                sp.setSpawning(true);
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(or, new Runnable() {

                    @Override
                    public void run() {
                        sp.spawnEntity();
                        sp.setSpawning(false);
                    }
                }, sp.getDelay() * 20);
            } else {
            }
        }
    }
}