package com.voidtechnologies.openrift.dungeons;

import com.voidtechnologies.openrift.Attribute;
import com.voidtechnologies.openrift.OpenRift;
import static com.voidtechnologies.openrift.OpenRift.log;
import com.voidtechnologies.openrift.schematics.Schematic;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 *
 * @author dylan
 */
public class RiftRaid {

    private HashMap<Attribute, Integer> attributes = new HashMap<>();
    private LinkedList<Portal> portals = new LinkedList<>(), exitPortals = new LinkedList<>();
    private LinkedList<LivingEntity> bosses = new LinkedList<>();
    private LinkedList<Location> bossSpawns = new LinkedList<>();
    private int closeTime, lootModifier;
    private World world;
    private OpenRift plugin;
    private Schematic schem;

    public RiftRaid(String schematic, OpenRift plugin) {
        this.plugin = plugin;
        schem = null;
        world = Bukkit.getWorld("OpenRift_World");
        try {
            log("Loading schem...");
            schem = Schematic.loadSchematic(new File(plugin.getDataFolder() + "/" + schematic + ".schematic"));
        } catch (IOException ex) {
            log("Failed to load schematic, IOE");
            Logger.getLogger(RiftRaid.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (schem == null) {
            log("Schem equals null returning");
            return;
        }
        log("Pasting schematic...");
        schem.pasteSchematic(new Location(world, 0, 1, 0));

        closeTime = plugin.getConfig().getInt("dungeons." + schematic + ".close.time");
        lootModifier = plugin.getConfig().getInt("dungeons." + schematic + ".boss.loot", 0);

        HashMap<Attribute, Integer> attrs = new HashMap<>();

        ConfigurationSection section = plugin.getConfig().getConfigurationSection("dungeons." + schematic + ".attributes");
        if (section != null) {
            for (String attr : section.getKeys(false)) {
                attrs.put(Attribute.valueOf(attr), plugin.getConfig().getInt("dungeons." + schematic + ".attributes." + attr));
            }
            attributes = attrs;
        }
        if (attributes.isEmpty()) {
            generateAttributes();
        }

        log("Setting up spawns...");
        generateLocations(plugin.getConfig(), schematic);
        generateSpawnPoints(plugin.getConfig(), schematic);
        generateBosses(plugin.getConfig(), schematic);
        log("Spawning boss");

        log("Setting OpenRift.setRIft to this instance of Rift");
        OpenRift.setRift(this);
    }

    public static double[] normalizePlayerCoords() {
        double[] ret = new double[2];
        double x = 0;
        double z = 0;
        int i = 0;

        for (Player player : Bukkit.getWorlds().get(0).getPlayers()) {
            x += player.getLocation().getX();
            z += player.getLocation().getZ();
            i++;
        }
        if (i == 0) {
            ret[0] = 0;
            ret[1] = 0;
        } else {
            ret[0] = x / i;
            ret[1] = z / i;
        }
        return ret;
    }

    public Schematic getSchem() {
        return schem;
    }

    private void generateBosses(FileConfiguration config, String schematic) {
        LinkedList<LivingEntity> tempBosses = new LinkedList<>();
        int i = 1;
        for (Location loc : bossSpawns) {
            LivingEntity boss = null;
            String type = config.getString("dungeons." + schematic + ".bosses.BOSS" + i + ".type");
            System.out.println("TYPE: " + type);
            EntityType et = EntityType.valueOf(type);
            int health = config.getInt("dungeons." + schematic + ".bosses.BOSS" + i + ".health");
            String name = config.getString("dungeons." + schematic + ".bosses.BOSS" + i + ".name");

            boss = (LivingEntity) world.spawnEntity(loc, et);
            boss.setCustomName(name);
            boss.setMaxHealth(health);
            boss.setHealth(health);
            boss.setCustomNameVisible(true);
            tempBosses.add(boss);
            i++;
        }
        bosses = tempBosses;
    }

    private void generateAttributes() {
        Random rand = new Random();
        int amount = Attribute.randomDegree(10) + 3;
        for (int i = 0; i < amount; i++) {
            Attribute attr = Attribute.values()[rand.nextInt(Attribute.values().length)];
            if (attributes.containsKey(attr)) {
                i--;
                continue;
            }
            attributes.put(attr, Attribute.randomDegree(attr.getMax()));
        }
    }

    public HashMap<Attribute, Integer> getAttributes() {
        return attributes;
    }

    public int getLootModifier() {
        return lootModifier;
    }

    private void generateLocations(FileConfiguration config, String dungeon) {
        log("Getting offsets");

        ConfigurationSection spawns = config.getConfigurationSection("dungeons." + dungeon + ".bosses");
        if (spawns != null) {
            for (String key : spawns.getKeys(false)) {
                if (key == null | key.equalsIgnoreCase("")) {
                    continue;
                }
                double xx = config.getDouble("dungeons." + dungeon + ".bosses." + key + ".x");
                double yy = config.getDouble("dungeons." + dungeon + ".bosses." + key + ".y");
                double zz = config.getDouble("dungeons." + dungeon + ".bosses." + key + ".z");
                bossSpawns.add(new Location(world, xx, yy + 1, zz));
            }
        }

        for (int i = 1; i <= 5; i++) {
            double[] norm = normalizePlayerCoords();
            double x = config.getDouble("dungeons." + dungeon + ".spawn." + i + ".x");
            double y = config.getDouble("dungeons." + dungeon + ".spawn." + i + ".y");
            double z = config.getDouble("dungeons." + dungeon + ".spawn." + i + ".z");
            Random rand = new Random();
            int xr = rand.nextInt(2000) - 1000;
            int zr = rand.nextInt(2000) - 1000;

            Location tmp = new Location(Bukkit.getWorlds().get(0), norm[0] + xr, 0, norm[1] + zr);
            Block portalLoc = Bukkit.getWorlds().get(0).getHighestBlockAt(tmp.getBlockX(), tmp.getBlockZ());
            log("Constructing portal...");
            System.out.println("Constructed Portal Output at: " + x + " - " + (y) + " - " + z);
            portals.add(new Portal(portalLoc, new Location(world, x, y, z), plugin, "TOTEM", true, this));
        }
    }

    private void generateSpawnPoints(FileConfiguration config, String dungeon) {
        ConfigurationSection configurationSection = config.getConfigurationSection("dungeons." + dungeon + ".spawnpoints");
        if (configurationSection == null) {
            return;
        }

        for (String key : configurationSection.getKeys(false)) {
            log("Creating spawnpoint: " + key);
            double x = config.getDouble("dungeons." + dungeon + ".spawnpoints." + key + ".x");
            double y = config.getDouble("dungeons." + dungeon + ".spawnpoints." + key + ".y");
            double z = config.getDouble("dungeons." + dungeon + ".spawnpoints." + key + ".z");
            Location loc = new Location(world, x, y, z);

            int radius = config.getInt("dungeons." + dungeon + ".spawnpoints." + key + ".radius");
            int delay = config.getInt("dungeons." + dungeon + ".spawnpoints." + key + ".delay");
            int amount = config.getInt("dungeons." + dungeon + ".spawnpoints." + key + ".amount");
            int maxHealth = config.getInt("dungeons." + dungeon + ".spawnpoints." + key + ".maxHealth");
            int minHealth = config.getInt("dungeons." + dungeon + ".spawnpoints." + key + ".minHealth");

            int maxAmt = config.getInt("dungeons." + dungeon + ".spawnpoints." + key + ".maxAmount");
            String[] sEntities = config.getString("dungeons." + dungeon + ".spawnpoints." + key + ".type").split(",");
            EntityType[] types = new EntityType[sEntities.length];
            for (int i = 0; i < sEntities.length; i++) {
                types[i] = EntityType.fromName(sEntities[i]);
            }
            SpawnPoint point = new SpawnPoint(loc, types, amount, delay, radius, maxHealth, minHealth, maxAmt);
        }
    }

    public void destroy(boolean restart) {
        Map<Location, SpawnPoint> spawns = SpawnPoint.getSpawns();
        for (SpawnPoint spawn : spawns.values()) {
            spawn.getEntities().clear();
        }
        SpawnPoint.getSpawns().clear();
        for (Portal portal : portals) {
            portal.destroyPortal(restart);
        }
        for (Portal exitPortal : exitPortals) {
            exitPortal.destroyPortal(true);
        }
        attributes.clear();
        for (Entity le : world.getEntities()) {
            if (le instanceof LivingEntity) {
                ((LivingEntity) le).setHealth(0.0);
                continue;
            }
            le.remove();
        }
    }

    public double getLootValue() {
        double total = 0.0;
        for (Attribute att : attributes.keySet()) {
            total += att.getModifier();
        }
        return total;
    }

    public LinkedList<Portal> getPortals() {
        return portals;
    }

    public int getCloseTime() {
        return closeTime;
    }

    public LinkedList<LivingEntity> getBosses() {
        return bosses;
    }

    public void summonExitPortals(OpenRift plugin) {
        Block eBlock = portals.get(0).getExitLocation().getBlock();
        for (Portal portal : portals) {
            exitPortals.add(new Portal(portal.getExitLocation().getBlock(), portal.getLocation(), plugin, "REDSTONE", false, this));
        }
    }
}
