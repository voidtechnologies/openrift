package com.voidtechnologies.openrift.dungeons;

import com.sainttx.holograms.api.Hologram;
import com.sainttx.holograms.api.HologramPlugin;
import com.sainttx.holograms.api.line.TextLine;
import com.voidtechnologies.openrift.Attribute;
import static com.voidtechnologies.openrift.ItemManager.*;
import com.voidtechnologies.openrift.OpenRift;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author dylan
 */
public class Portal {

    private boolean exact, enabled;
    private Block block;
    private Location location, exitLocation;
    private OpenRift or;
    private int taskID = 0;
    private String part;
    private RiftRaid rift;

    public Portal(final Block block, Location exitLocation, OpenRift or, String particle, boolean bool, RiftRaid rift) {
        this.exitLocation = exitLocation;
        this.part = particle;
        this.rift = rift;
        this.location = block.getLocation();
        this.block = block;
        this.or = or;

        final double xx = location.getX() + 0.5;
        final double yy = location.getY() + 1.25;
        final double zz = location.getZ() + 0.5;

        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(or, new Runnable() {
            double x = 0;
            double y = 0;
            double theta = Math.PI / 2;
            double r = 0;
            double z = 0;
            int i = 0;

            @Override
            public void run() {

                r = .025 * theta;
                x = r * Math.cos(theta);
                y = r * Math.sin(theta);
                block.getWorld().spawnParticle(Particle.valueOf(part), new Location(location.getWorld(), xx + x, yy + z, zz + y), 0);
                theta += Math.PI / 5;
                z += .01;
                if (i == 60) {
                    theta = Math.PI / 2;
                    i = 0;
                    z = 0;
                }
                i++;
            }
        }, 20L, 1L);
        spawnPortal(bool);
        enabled = true;
        OpenRift.portals.add(this);
        if (bool) {
            Bukkit.broadcastMessage("Generating the portal at: " + block.getX() + ", " + block.getY() + ", " + block.getZ());
        }
    }

    public Location getExitLocation() {
        return exitLocation;
    }

    public void setExitLocation(Location lctn) {
        this.exitLocation = lctn;
    }

    public Block getBlock() {
        return block;
    }

    public Location getLocation() {
        return location;
    }

    public void spawnPortal(boolean bool) {
        block.setType(Material.ENDER_PORTAL_FRAME);
        if (bool) {
            Hologram holo = new Hologram("portal_notification" + block.getLocation().toString(), block.getLocation().clone().add(0.5, 2, 0.5));
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            for (Entry<Attribute, Integer> attr : rift.getAttributes().entrySet()) {
                sb.append(attr.getKey().toNiceName()).append(": ").append(attr.getValue()).append(",");
            }
            sb.replace(sb.lastIndexOf(","), sb.length(), "]");
            holo.addLine(new TextLine(holo, sb.toString()));
            JavaPlugin.getPlugin(HologramPlugin.class).getHologramManager().addActiveHologram(holo);
        }
    }

    public void destroyPortal(boolean restart) {
        Bukkit.getScheduler().cancelTask(taskID);
        block.setType(Material.AIR);

        HologramPlugin jp = JavaPlugin.getPlugin(HologramPlugin.class);
        if (jp != null && jp.getHologramManager() != null) {
            jp.getHologramManager().clear();
        }

        if (restart) {
            return;
        }
        Bukkit.broadcastMessage("The portal has closed...");

        Random random = new Random();

        int amountItems = random.nextInt(rift.getAttributes().size()) + 1;
        if (rift.getAttributes().containsKey(Attribute.EXTRA_LOOT)) {
            amountItems += 1;
        }

        amountItems += rift.getLootModifier();

        LinkedList<Integer> dropTables = new LinkedList<>();

        for (int i = 0; i < amountItems; i++) {
            double rn = random.nextDouble() * 100.0;
            if (rn < LEGENDARY_RATE) {
                System.out.println("LEGENDARY TABLE");
                if (dropTables.contains(LEGENDARY)) {
                    dropTables.add(EPIC);
                    continue;
                }
                dropTables.add(LEGENDARY);
            } else if (rn < EPIC_RATE) {
                System.out.println("EPIC TABLE");
                if (dropTables.contains(EPIC)) {
                    dropTables.add(RARE);
                    continue;
                }
                dropTables.add(EPIC);
            } else if (rn < RARE_RATE) {
                System.out.println("RARE TABLE");
                if (dropTables.contains(RARE)) {
                    dropTables.add(COMMON);
                    continue;
                }
                dropTables.add(RARE);
            } else {
                System.out.println("COMMON TABLE");
                dropTables.add(COMMON);
            }
        }

        List<ItemStack> items = new ArrayList<>();
        List<ItemStack> common = or.getItemManager().getCommonDrops(), rare = or.getItemManager().getRareDrops(), epic = or.getItemManager().getEpicDrops(),
                legendary = or.getItemManager().getLegendaryDrops();
        for (int table : dropTables) {
            ItemStack toDrop;
            switch (table) {
                case COMMON:
                    toDrop = common.get(random.nextInt(common.size()));
                    System.out.println("Dropping common item: " + toDrop.getType().name() + ":" + toDrop.getAmount());
                    items.add(toDrop);
                    break;
                case RARE:
                    toDrop = rare.get(random.nextInt(rare.size()));
                    System.out.println("Dropping rare item: " + toDrop.getType().name() + ":" + toDrop.getAmount());
                    items.add(toDrop);
                    break;
                case EPIC:
                    toDrop = epic.get(random.nextInt(epic.size()));
                    System.out.println("Dropping epic item: " + toDrop.getType().name() + ":" + toDrop.getAmount());
                    items.add(toDrop);
                    break;
                case LEGENDARY:
                    toDrop = legendary.get(random.nextInt(legendary.size()));
                    System.out.println("Dropping legendary item: " + toDrop.getType().name() + ":" + toDrop.getAmount());
                    items.add(toDrop);
                    break;
            }
        }

        for (ItemStack is : items) {
            block.getWorld().dropItem(location, is);
        }
    }

    public void setEnabled(boolean bool) {
        enabled = bool;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
