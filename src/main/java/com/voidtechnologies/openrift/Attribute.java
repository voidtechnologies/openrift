package com.voidtechnologies.openrift;

import static com.voidtechnologies.openrift.AttributeType.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author dylan
 */
public enum Attribute {

    REGEN("Regeneration", ON_SPAWN, 3, 0.25),
    FAST("Fast", ON_SPAWN, 4, 0.25),
    EXTRA_HEALTH("Extra Health", ON_SPAWN, 5, 0.25),
    THORNS("Thorns", ON_HIT, 1, 0.5),
    POISON("Poisoned", ON_DAMAGE, 3, 0.75),
    EXTRA_LOOT("Extra Loot", ON_DEATH, 3, 1.5),
    FIRE_DAMAGE("Fiery Avatar", ON_DAMAGE, 3, 0.5),
    EXTRA_DAMAGE("Extra Damage", ON_DAMAGE, 3, 0.75),
    WITHER("Wither", ON_DAMAGE, 4, 0.75),
    LIGHTNING_DAMAGE("Lightning", ON_DAMAGE, 2, 0.75),
    EXPLOSIVE_DEATH("Explosive Death", ON_DEATH, 1, 1.0);

    private String niceName;
    private AttributeType type;
    private int max;
    private double modifier;

    private Attribute(String niceName, AttributeType type, int max, double modifier) {
        this.niceName = niceName;
        this.type = type;
        this.max = max;
        this.modifier = modifier;
    }

    public Attribute valueOfNiceName(String nName) {
        for (Attribute attribute : Attribute.values()) {
            if (attribute.niceName.equalsIgnoreCase(nName)) {
                return attribute;
            }
        }
        return null;
    }

    public String toNiceName() {
        return niceName;
    }

    public AttributeType getType() {
        return type;
    }

    public int getMax() {
        return max;
    }

    public double getModifier() {
        return modifier;
    }

    public static List<Attribute> getAttributes(Set<Attribute> attrs, AttributeType type) {
        List<Attribute> ret = new ArrayList<>();
        for (Attribute atrr : attrs) {
            if (atrr.getType().equals(type)) {
                ret.add(atrr);
            }

        }
        return ret;
    }

    public static void applySpawnAttribute(LivingEntity entity, Attribute attribute, int degree) {
        switch (attribute) {
            case EXTRA_HEALTH:
                entity.setMaxHealth(entity.getMaxHealth() * (1 + (degree * .5)));
                entity.setHealth(entity.getMaxHealth());
                break;
            case FAST:
                entity.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, degree));
                break;
            case REGEN:
                entity.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, degree));
                break;
        }
    }

    public static void applyOnHitAttribute(LivingEntity entity, Attribute attribute, int degree, EntityDamageByEntityEvent e) {
        switch (attribute) {
            case THORNS:
                entity.damage(e.getDamage() * .25);
                break;
        }
    }

    public static void applyOnDamageAttribute(LivingEntity entity, Attribute attribute, int degree, EntityDamageByEntityEvent e) {
        switch (attribute) {
            case FIRE_DAMAGE:
                entity.setFireTicks(40 * (degree + 1));
                break;
            case POISON:
                entity.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 60, degree));
                break;
            case EXTRA_DAMAGE:
                double dmg = e.getDamage();
                e.setDamage((1 + (.25 * degree)) * dmg);
                break;
            case WITHER:
                entity.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 50, degree));
                break;
            case LIGHTNING_DAMAGE:
                for (int i = 0; i < degree; i++) {
                    entity.getWorld().strikeLightning(entity.getLocation());
                }
                break;
        }
    }

    public static void applyOnDeathAttribute(Attribute attribute, int degree, EntityDeathEvent event) {
        switch (attribute) {
            case EXPLOSIVE_DEATH:
                event.getEntity().getLocation().getWorld().createExplosion(event.getEntity().getLocation(), degree);
                break;
        }
    }

    public static int randomDegree(int max) {
        Random rand = new Random();
        return (max + 1) - (int) Math.sqrt(rand.nextInt((int) Math.pow(max + 1, 2) - 1) + 1);
    }

    public static int maxLootTable(Map<Attribute, Integer> map) {
        double x = 0;
        for (Entry<Attribute, Integer> attributes : map.entrySet()) {
            x += (attributes.getKey().getModifier() * attributes.getValue());
        }
        return (int) Math.ceil(x);
    }
}
