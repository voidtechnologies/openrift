package com.voidtechnologies.openrift;

import com.voidtechnologies.openrift.dungeons.Portal;
import com.voidtechnologies.openrift.dungeons.RiftRaid;
import com.voidtechnologies.openrift.dungeons.SpawnPointTask;
import com.voidtechnologies.openrift.listeners.ORListener;
import com.voidtechnologies.openrift.mechanics.SwapPlayers;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author dylan
 */
public class OpenRift extends JavaPlugin {

    private static final Logger LOGGER = Logger.getLogger("Minecraft");
    private static RiftRaid rift;
    public static List<Portal> portals = new ArrayList<>();
    private SpawnPortalTask spt;
    private static OpenRift instance;
    private ItemManager im;

    @Override
    public void onEnable() {
        ORListener orl = new ORListener(this);
        im = new ItemManager();
        im.loadItems(YamlConfiguration.loadConfiguration(new File(getDataFolder() + "/items.yml")));
        World riftWorld = Bukkit.getWorld("OpenRift_World");
        if (riftWorld == null) {
            if (new File(getDataFolder().getParentFile().getParent() + "/OpenRift_World").exists()) {
                log("OpenRift_World exists, but is not loaded. loading...");
                riftWorld = getServer().createWorld(new WorldCreator("OpenRift_World"));
            } else {
                log(Level.INFO, "OpenRift_World did not exist, generating world...");

                WorldCreator creator = new WorldCreator("OpenRift_World");
                creator.generator(new EmptyChunkGenerator());
                riftWorld = creator.createWorld();
                log(Level.INFO, "World has been generated.");
            }
        }

        riftWorld.setSpawnLocation(0, 1, 0);
        spt = new SpawnPortalTask(this);
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, spt, 240L, 1200L);
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new SpawnPointTask(this), 300L, 20L);
        try {
            this.getConfig().load(this.getDataFolder() + "/config.yml");
        } catch (IOException ex) {
            Logger.getLogger(OpenRift.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidConfigurationException ex) {
            Logger.getLogger(OpenRift.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (rift == null) {
            riftWorld.getLivingEntities().clear();
            for (LivingEntity le : riftWorld.getLivingEntities()) {
                le.remove();
            }
        }

        this.getCommand("test").setExecutor(new CommandExecutor() {
            @Override
            public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {

                Player player = (Player) cs;
                int amount, duration, radius;
                SwapPlayers swap = new SwapPlayers((LivingEntity) player);
                swap.prepareSpell();
                return true;
            }
        });
        log("Enabled!");
        instance = this;
    }

    @Override
    public void onDisable() {
        if (rift != null) {
            rift.destroy(true);
        }
        log("Disabled!");
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        return new EmptyChunkGenerator();
    }

    public static void log(String message) {
        LOGGER.log(Level.INFO, "[OpenRift] {0}", message);
    }

    public static void log(Level level, String message) {
        LOGGER.log(level, "[OpenRift] {0}", message);
    }

    public static Portal isInPortal(Player player) {
        if (portals.isEmpty()) {
            return null;
        }
        for (Portal portal : portals) {
            if (!portal.isEnabled()) {
                continue;
            }
            if (!portal.getLocation().getWorld().equals(player.getWorld())) {
                continue;
            }
            if (player.getLocation().distanceSquared(portal.getLocation()) < 5) {
                return portal;
            }
        }
        return null;
    }

    public static RiftRaid getRift() {
        return rift;
    }

    public static void setRift(RiftRaid portal) {
        OpenRift.rift = portal;
    }

    public SpawnPortalTask getSpt() {
        return spt;
    }

    public ItemManager getItemManager() {
        return im;
    }

    public static OpenRift getInstance() {
        return instance;
    }
}
