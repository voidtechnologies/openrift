
package com.voidtechnologies.openrift;

/**
 *
 * @author dylan
 */
public enum AttributeType {
    
    ON_SPAWN,
    PERSISTENT,
    ON_DAMAGE,
    ON_DEATH,
    ON_HIT,
    PASSIVE;
}