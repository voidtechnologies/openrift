package com.voidtechnologies.openrift.mechanics;

import com.voidtechnologies.openrift.OpenRift;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

/**
 *
 * @author dylan
 */
public class LightningSurge extends Spell {

    private Collection<Entity> entities;
    private LinkedList<LivingEntity> targets;
    private int maxCasts;

    public LightningSurge(LivingEntity caster, int maxCasts) {
        super(caster);
        this.maxCasts = maxCasts;
        entities = caster.getWorld().getNearbyEntities(caster.getLocation(), 10, 5, 10);
    }

    @Override
    public void prepareSpell() {
        targets = new LinkedList<>();
        for (Entity entity : entities) {
            if (entity instanceof LivingEntity) {
                targets.add((LivingEntity) entity);
                continue;
            }
        }

        cast();
    }

    @Override
    public void cast() {
        Random rng = new Random();
        while (maxCasts > 0) {
            int index = rng.nextInt(targets.size());
            final LivingEntity target = targets.get(index);
            Bukkit.getScheduler().scheduleSyncDelayedTask(OpenRift.getInstance(), new Runnable() {
                @Override
                public void run() {
                    new LightningBolt(caster, target).prepareSpell();
                }
            }, rng.nextInt(2) * 20L);
            maxCasts--;
        }
    }
}
