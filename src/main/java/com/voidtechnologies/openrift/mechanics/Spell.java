package com.voidtechnologies.openrift.mechanics;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

/**
 *
 * @author dylan
 */
public abstract class Spell implements Castable {

    protected LivingEntity caster;
    protected LivingEntity target;
    protected Location area;

    public Spell(LivingEntity caster) {
        this.caster = caster;
    }

    public Spell(LivingEntity caster, LivingEntity target) {
        this.caster = caster;
        this.target = target;
    }

    public Spell(LivingEntity caster, Location area) {
        this.caster = caster;
        this.area = area;
    }

    public Spell(LivingEntity caster, LivingEntity target, Location area) {
        this.caster = caster;
        this.area = area;
        this.target = target;
    }

    public void prepareSpell() {
        cast();
    }
}
