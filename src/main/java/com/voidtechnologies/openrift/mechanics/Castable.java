package com.voidtechnologies.openrift.mechanics;

/**
 *
 * @author dylan
 */
public interface Castable {
    
    public void cast();
    
}