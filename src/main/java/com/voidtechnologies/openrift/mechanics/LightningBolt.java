package com.voidtechnologies.openrift.mechanics;

import org.bukkit.EntityEffect;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;

/**
 *
 * @author dylan
 */
public class LightningBolt extends Spell {

    public LightningBolt(LivingEntity caster, LivingEntity target) {
        super(caster, target);
    }

    @Override
    public void prepareSpell() {
        caster.playEffect(EntityEffect.FIREWORK_EXPLODE);

        caster.getWorld().playSound(caster.getLocation(), Sound.ENTITY_ELDER_GUARDIAN_CURSE, 5, 1);
        LightningBolt.super.prepareSpell();
    }

    @Override
    public void cast() {
        caster.getWorld().strikeLightning(target.getLocation());
        target.damage(30, caster);
    }
}
