package com.voidtechnologies.openrift.mechanics;

import com.voidtechnologies.openrift.OpenRift;
import java.util.Collection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

/**
 *
 * @author dylan
 */
public class FireWave extends Spell {

    public FireWave(LivingEntity caster) {
        super(caster);
    }

    @Override
    public void prepareSpell() {
        super.prepareSpell();
    }

    @Override
    public void cast() {
        Location location = caster.getLocation();
        final double xx = location.getX() + 0.5;
        final double yy = location.getY() + 0.25;
        final double zz = location.getZ() + 0.5;
        final int taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(OpenRift.getInstance(), new Runnable() {
            double x = 0, y = 0, theta = Math.PI / 2, r = 0;
            int i = 0;
            Block prevBlock = null;

            @Override
            public void run() {
                r = .45 * theta;
                x = r * Math.cos(theta);
                y = r * Math.sin(theta);

                Location loc = new Location(caster.getWorld(), xx + x, yy, zz + y);
                Block block = loc.getBlock();
                if (block.getType().equals(Material.AIR)) {
                    block.setType(Material.FIRE);
                }
                burn(caster.getWorld().getNearbyEntities(loc, 1.25, 1, 1.25));
                //if (prevBlock != null && prevBlock.getType().equals(Material.FIRE)) {
                  //  prevBlock.setType(Material.AIR);
                //}
                prevBlock = block;

                theta += Math.PI / 6;
                if (i == 80) {
                    theta = Math.PI / 2;
                    i = 0;
                }
                i++;
            }
        }, 20L, 1L);

        Bukkit.getScheduler().scheduleSyncDelayedTask(OpenRift.getInstance(), new Runnable() {
            @Override
            public void run() {
                Bukkit.getScheduler().cancelTask(taskId);
            }
        }, 81L);
    }

    private void burn(Collection<Entity> entities) {
        for (Entity entity : entities) {
            if (!(entity instanceof LivingEntity)) {
                continue;
            }
            ((LivingEntity) entity).damage(15, caster);
        }
    }
}
