package com.voidtechnologies.openrift.mechanics;

import com.voidtechnologies.openrift.OpenRift;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 *
 * @author dylan
 */
public class SwapPlayers extends Spell {

    private List<Player> players = new ArrayList<>();

    public SwapPlayers(LivingEntity caster) {
        super(caster);
    }

    @Override
    public void prepareSpell() {
        caster.getWorld().playSound(caster.getLocation(), Sound.ENTITY_ENDERMEN_STARE, 1, 20);
        List<Entity> nearbyEntities = caster.getNearbyEntities(15, 3, 15);
        for (Entity entity : nearbyEntities) {
            if (entity instanceof Player) {
                players.add((Player) entity);
            }
        }
        super.prepareSpell();
    }

    @Override
    public void cast() {
        Random rng = new Random();
        for (int i = 0; i < players.size(); i++) {
            Collections.swap(players, i, rng.nextInt(players.size()));

        }
        final int taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(OpenRift.getInstance(), new Runnable() {

            private int x = 0;

            @Override
            public void run() {
                if (!(x < players.size())) {
                    return;
                }
                Player player = players.get(x);

                if ((x + 1) == players.size()) {
                    player.teleport(caster);
                    caster.getWorld().playSound(caster.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 5);
                    return;
                }
                Player oPlayer = players.get(x + 1);
                Location loc = player.getLocation().clone();
                player.teleport(oPlayer);
                oPlayer.teleport(loc);
                caster.getWorld().playSound(caster.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 5);
                x++;
            }
        }, 10L, 15L);

        Bukkit.getScheduler().scheduleSyncDelayedTask(OpenRift.getInstance(), new Runnable() {
            @Override
            public void run() {
                Bukkit.getScheduler().cancelTask(taskId);
            }
        }, (15L * players.size()) + 10L);
    }
}
