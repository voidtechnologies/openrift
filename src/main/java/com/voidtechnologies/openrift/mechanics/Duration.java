package com.voidtechnologies.openrift.mechanics;

/**
 *
 * @author dylan
 */
public interface Duration {

    public void setDuration(int duration);

    public int getDuration();
}
