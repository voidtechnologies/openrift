package com.voidtechnologies.openrift.mechanics;

import com.voidtechnologies.openrift.OpenRift;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

/**
 *
 * @author dylan
 */
public class SummonAdds extends Spell implements Duration {

    private EntityType[] types;
    private int duration, amount, radius;

    public SummonAdds(LivingEntity entity, EntityType[] types, int amount, int duration, int radius) {
        super(entity);
        this.types = types;
        this.amount = amount;
        this.duration = duration;
        this.radius = radius;
    }

    @Override
    public void prepareSpell() {
        caster.getWorld().playSound(caster.getLocation(), Sound.ENTITY_VEX_CHARGE, 1, 10);
        super.prepareSpell();
    }

    @Override
    public void cast() {
        final Random rng = new Random();
        long ticks = (duration / amount * 20L);
        Invulnerable invul = new Invulnerable(caster, duration);
        invul.prepareSpell();
        final int taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(OpenRift.getInstance(), new Runnable() {
            @Override
            public void run() {
                Location l = caster.getLocation().clone();
                l.add(rng.nextInt(radius), 0, rng.nextInt(radius));
                Location ol = l.clone().add(0, 1, 0);
                while (l.getBlock().getType().isSolid() && ol.getBlock().getType().isSolid()) {
                    l = caster.getLocation().clone();
                    l.add(rng.nextInt(radius), 0, rng.nextInt(radius));
                    ol = l.clone().add(0, 1, 0);
                }
                caster.getWorld().playSound(l, Sound.ENTITY_BLAZE_SHOOT, (float) 0.6, 4);
                caster.getWorld().spawnEntity(l, types[rng.nextInt(types.length)]);
            }
        }, 20L, ticks);
        Bukkit.getScheduler().scheduleSyncDelayedTask(OpenRift.getInstance(), new Runnable() {
            @Override
            public void run() {
                Bukkit.getScheduler().cancelTask(taskId);
            }
        }, (duration + 1) * 20L);

    }

    @Override
    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public int getDuration() {
        return duration;
    }
}
