package com.voidtechnologies.openrift.mechanics;

import com.voidtechnologies.openrift.OpenRift;
import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;

/**
 *
 * @author dylan
 */
public class Invulnerable extends Spell implements Duration {

    private int duration = 0;

    public Invulnerable(LivingEntity caster, int duration) {
        super(caster);
        this.duration = duration;

    }

    @Override
    public void prepareSpell() {
        caster.getWorld().playSound(caster.getLocation(), Sound.ENTITY_GHAST_SCREAM, 1.0F, 10F);
        caster.getWorld().spawnParticle(Particle.FALLING_DUST, 10, 3, 10, 100);
        super.prepareSpell();
    }

    @Override
    public void cast() {
        caster.setInvulnerable(true);
        Bukkit.getScheduler().scheduleSyncDelayedTask(OpenRift.getInstance(), new Runnable() {
            @Override
            public void run() {
                caster.setInvulnerable(false);
            }
        }, duration * 20L);
    }

    @Override
    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public int getDuration() {
        return duration;
    }

}
